# recipe_app
[![Codemagic build status](https://api.codemagic.io/apps/60b36477c004537eee2c03aa/60b36477c004537eee2c03a9/status_badge.svg)](https://codemagic.io/apps/60b36477c004537eee2c03aa/60b36477c004537eee2c03a9/latest_build)

Displays recipes served via the [recipe-server](https://gitlab.com/enra64/recipe-scrapers-server) server.

