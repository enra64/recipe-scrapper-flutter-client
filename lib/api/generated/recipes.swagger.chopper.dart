// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recipes.swagger.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$Recipes extends Recipes {
  _$Recipes([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = Recipes;

  @override
  Future<Response<RecipeResponse>> convertRecipe({RecipeRequest? body}) {
    final $url = '/recipe';
    final $body = body;
    final $request = Request('PUT', $url, client.baseUrl, body: $body, headers: {"Authorization": "Basic c3VwZXJzZWNyZXQ6dXlDJlRDcWE+JmpBdmp5UG5CbzQ="});
    return client.send<RecipeResponse, RecipeResponse>($request);
  }
}
