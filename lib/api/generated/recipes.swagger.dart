import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';
import 'package:chopper/chopper.dart';
import 'package:chopper/chopper.dart' as chopper;

part 'recipes.swagger.chopper.dart';
part 'recipes.swagger.g.dart';

// **************************************************************************
// SwaggerChopperGenerator
// **************************************************************************

@ChopperApi()
abstract class Recipes extends ChopperService {
  static Recipes create([ChopperClient? client]) {
    if (client != null) {
      return _$Recipes(client);
    }

    final newClient = ChopperClient(
        services: [_$Recipes()],
        converter: JsonSerializableConverter(),
        baseUrl: 'https://recipes.herdick.info');
    return _$Recipes(newClient);
  }

  ///Request the conversion of a recipe
  ///@param body

  @Put(path: '/recipe')
  Future<chopper.Response<RecipeResponse>> convertRecipe(
      {@Body() @required RecipeRequest? body});
}

final Map<Type, Object Function(Map<String, dynamic>)>
    RecipesJsonDecoderMappings = { // ignore: non_constant_identifier_names
  RecipeRequest: RecipeRequest.fromJsonFactory,
  RecipeResponse: RecipeResponse.fromJsonFactory,
};

@JsonSerializable(explicitToJson: true)
class RecipeRequest {
  RecipeRequest({
    this.url,
  });

  factory RecipeRequest.fromJson(Map<String, dynamic> json) =>
      _$RecipeRequestFromJson(json);

  @JsonKey(name: 'url')
  final String? url;
  static const fromJsonFactory = _$RecipeRequestFromJson;
  static const toJsonFactory = _$RecipeRequestToJson;
  Map<String, dynamic> toJson() => _$RecipeRequestToJson(this)
    ..removeWhere((key, value) => value is String && value.isEmpty);
}

extension $RecipeRequestExtension on RecipeRequest {
  RecipeRequest copyWith({String? url}) {
    return RecipeRequest(url: url ?? this.url);
  }
}

@JsonSerializable(explicitToJson: true)
class RecipeResponse {
  RecipeResponse({
    this.image,
    this.name,
    this.instructions,
    this.prepTime,
    this.cookTime,
    this.totalTime,
    this.ratingCount,
    this.ratingValue,
    this.author,
    this.ingredients,
  });

  factory RecipeResponse.fromJson(Map<String, dynamic> json) =>
      _$RecipeResponseFromJson(json);

  @JsonKey(name: 'image')
  final String? image;
  @JsonKey(name: 'name')
  final String? name;
  @JsonKey(name: 'instructions')
  final String? instructions;
  @JsonKey(name: 'prepTime')
  final double? prepTime;
  @JsonKey(name: 'cookTime')
  final double? cookTime;
  @JsonKey(name: 'totalTime')
  final double? totalTime;
  @JsonKey(name: 'ratingCount')
  final double? ratingCount;
  @JsonKey(name: 'ratingValue')
  final double? ratingValue;
  @JsonKey(name: 'author')
  final String? author;
  @JsonKey(name: 'ingredients', defaultValue: <String>[])
  final List<String>? ingredients;
  static const fromJsonFactory = _$RecipeResponseFromJson;
  static const toJsonFactory = _$RecipeResponseToJson;
  Map<String, dynamic> toJson() => _$RecipeResponseToJson(this)
    ..removeWhere((key, value) => value is String && value.isEmpty);
}

extension $RecipeResponseExtension on RecipeResponse {
  RecipeResponse copyWith(
      {String? image,
      String? name,
      String? instructions,
      double? prepTime,
      double? cookTime,
      double? totalTime,
      double? ratingCount,
      double? ratingValue,
      String? author,
      List<String>? ingredients}) {
    return RecipeResponse(
        image: image ?? this.image,
        name: name ?? this.name,
        instructions: instructions ?? this.instructions,
        prepTime: prepTime ?? this.prepTime,
        cookTime: cookTime ?? this.cookTime,
        totalTime: totalTime ?? this.totalTime,
        ratingCount: ratingCount ?? this.ratingCount,
        ratingValue: ratingValue ?? this.ratingValue,
        author: author ?? this.author,
        ingredients: ingredients ?? this.ingredients);
  }
}

typedef JsonFactory<T> = T Function(Map<String, dynamic> json);

class CustomJsonDecoder {
  CustomJsonDecoder(this.factories);

  final Map<Type, JsonFactory> factories;

  dynamic decode<T>(dynamic entity) {
    if (entity is Iterable) {
      return _decodeList<T>(entity);
    }

    if (entity is T) {
      return entity;
    }

    if (entity is Map<String, dynamic>) {
      return _decodeMap<T>(entity);
    }

    return entity;
  }

  T _decodeMap<T>(Map<String, dynamic> values) {
    final jsonFactory = factories[T];
    if (jsonFactory == null || jsonFactory is! JsonFactory<T>) {
      return throw "Could not find factory for type $T. Is '$T: $T.fromJsonFactory' included in the CustomJsonDecoder instance creation in bootstrapper.dart?";
    }

    return jsonFactory(values);
  }

  List<T> _decodeList<T>(Iterable values) =>
      values.where((v) => v != null).map<T>((v) => decode<T>(v) as T).toList();
}

class JsonSerializableConverter extends chopper.JsonConverter {
  @override
  chopper.Response<ResultType> convertResponse<ResultType, Item>(
      chopper.Response response) {
    if (response.bodyString.isEmpty) {
      // In rare cases, when let's say 204 (no content) is returned -
      // we cannot decode the missing json with the result type specified
      return chopper.Response(response.base, null, error: response.error);
    }

    final jsonRes = super.convertResponse(response);
    return jsonRes.copyWith<ResultType>(
        body: jsonDecoder.decode<Item>(jsonRes.body) as ResultType);
  }
}

final jsonDecoder = CustomJsonDecoder(RecipesJsonDecoderMappings);

// ignore: unused_element
String? _dateToJson(DateTime? date) {
  if (date == null) {
    return null;
  }

  final year = date.year.toString();
  final month = date.month < 10 ? '0${date.month}' : date.month.toString();
  final day = date.day < 10 ? '0${date.day}' : date.day.toString();

  return '$year-$month-$day';
}
