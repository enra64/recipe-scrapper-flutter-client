// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recipes.swagger.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RecipeRequest _$RecipeRequestFromJson(Map<String, dynamic> json) {
  return RecipeRequest(
    url: json['url'] as String?,
  );
}

Map<String, dynamic> _$RecipeRequestToJson(RecipeRequest instance) =>
    <String, dynamic>{
      'url': instance.url,
    };

RecipeResponse _$RecipeResponseFromJson(Map<String, dynamic> json) {
  return RecipeResponse(
    image: json['image'] as String?,
    name: json['name'] as String?,
    instructions: json['instructions'] as String?,
    prepTime: (json['prepTime'] as num?)?.toDouble(),
    cookTime: (json['cookTime'] as num?)?.toDouble(),
    totalTime: (json['totalTime'] as num?)?.toDouble(),
    ratingCount: (json['ratingCount'] as num?)?.toDouble(),
    ratingValue: (json['ratingValue'] as num?)?.toDouble(),
    author: json['author'] as String?,
    ingredients: (json['ingredients'] as List<dynamic>?)
            ?.map((e) => e as String)
            .toList() ??
        [],
  );
}

Map<String, dynamic> _$RecipeResponseToJson(RecipeResponse instance) =>
    <String, dynamic>{
      'image': instance.image,
      'name': instance.name,
      'instructions': instance.instructions,
      'prepTime': instance.prepTime,
      'cookTime': instance.cookTime,
      'totalTime': instance.totalTime,
      'ratingCount': instance.ratingCount,
      'ratingValue': instance.ratingValue,
      'author': instance.author,
      'ingredients': instance.ingredients,
    };
