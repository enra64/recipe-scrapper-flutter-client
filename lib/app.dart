import 'package:flutter/material.dart';
import 'package:recipe_app/clipboard_ingestion/bloc/clipboard_bloc.dart';
import 'package:recipe_app/recipe/view/recipe_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recipe_app/recipe/recipe.dart';

class App extends MaterialApp {
  App()
      : super(
            home: MultiBlocProvider(providers: [
          BlocProvider(create: (_) => RecipeBloc()),
          BlocProvider(
              create: (_) => ClipboardBloc()..add(ClipboardLoadRequested()))
        ], child: RecipePage()));
}
