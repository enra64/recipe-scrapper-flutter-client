import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/services.dart';

part 'clipboard_event.dart';

part 'clipboard_state.dart';

class ClipboardBloc extends Bloc<ClipboardEvent, ClipboardState> {
  ClipboardBloc() : super(ClipboardState());

  @override
  Stream<ClipboardState> mapEventToState(ClipboardEvent event) async* {
    if (event is ClipboardLoadRequested) {
      yield await _getClipboard(state);
    }
  }

  Future<ClipboardState> _getClipboard(ClipboardState state) async {
    try {
      final clipboardContent = await Clipboard.getData('text/plain');
      final text = clipboardContent?.text;
      if (text == null || !text.contains("http")) {
        return state.copyWith(status: ClipboardRetrievalStatus.noUrl);
      }

      return state.copyWith(
          status: ClipboardRetrievalStatus.success, clipboardContent: text);
    } on Exception {
      return state.copyWith(status: ClipboardRetrievalStatus.failure);
    }
  }
}
