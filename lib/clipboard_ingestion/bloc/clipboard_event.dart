part of 'clipboard_bloc.dart';

abstract class ClipboardEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class ClipboardLoadRequested extends ClipboardEvent {}