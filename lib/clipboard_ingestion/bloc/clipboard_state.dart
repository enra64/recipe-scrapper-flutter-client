part of 'clipboard_bloc.dart';

enum ClipboardRetrievalStatus { initial, success, noUrl, failure }

class ClipboardState extends Equatable {
  const ClipboardState({
    this.status = ClipboardRetrievalStatus.initial,
    this.clipboardContent,
  });

  final ClipboardRetrievalStatus status;
  final String? clipboardContent;

  ClipboardState copyWith({
    ClipboardRetrievalStatus? status,
    String? clipboardContent,
  }) {
    return ClipboardState(
      status: status ?? this.status,
      clipboardContent: clipboardContent ?? this.clipboardContent,
    );
  }

  @override
  String toString() {
    return '''ClipboardState { status: $status, recipe: $clipboardContent }''';
  }

  @override
  List<Object> get props => [status, clipboardContent ?? ""];
}
