import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:recipe_app/api/generated/recipes.swagger.dart';

part 'recipe_event.dart';

part 'recipe_state.dart';

class RecipeBloc extends Bloc<RecipeEvent, RecipeState> {
  final client = Recipes.create();

  RecipeBloc() : super(RecipeState());

  @override
  Stream<RecipeState> mapEventToState(RecipeEvent event) async* {
    if (event is RecipeRequested) {
      yield await _mapPostFetchedToState(state, event.url);
    }
  }

  Future<RecipeState> _mapPostFetchedToState(
      RecipeState state, String url) async {
    try {
      final response =
          await client.convertRecipe(body: RecipeRequest(url: url));
      final recipe = response.body;
      return state.copyWith(
          status: RecipeRetrievalStatus.success, recipe: recipe);
    } on Exception {
      return state.copyWith(status: RecipeRetrievalStatus.failure);
    }
  }
}
