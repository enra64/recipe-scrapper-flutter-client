part of 'recipe_bloc.dart';

abstract class RecipeEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class RecipeRequested extends RecipeEvent {
  RecipeRequested({
    required this.url,
  });

  final String url;
}
