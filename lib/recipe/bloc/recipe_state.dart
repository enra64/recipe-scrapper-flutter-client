part of 'recipe_bloc.dart';

enum RecipeRetrievalStatus { initial, success, failure }

class RecipeState extends Equatable {
  const RecipeState({
    this.status = RecipeRetrievalStatus.initial,
    this.recipe,
  });

  final RecipeRetrievalStatus status;
  final RecipeResponse? recipe;

  RecipeState copyWith({
    RecipeRetrievalStatus? status,
    RecipeResponse? recipe,
  }) {
    return RecipeState(
      status: status ?? this.status,
      recipe: recipe ?? this.recipe,
    );
  }

  @override
  String toString() {
    return '''PostState { status: $status, recipe: $recipe }''';
  }

  @override
  List<Object> get props => [status, recipe ?? ""];
}
