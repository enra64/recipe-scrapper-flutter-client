import 'package:flutter/material.dart';

class RecipeAuthor extends StatelessWidget {
  const RecipeAuthor({this.author}) : super();

  final String? author;

  @override
  Widget build(BuildContext context) {
    final author = this.author;
    if (author == null) {
      return const Center(child: Text('Unknown Author'));
    }

    return RichText(
      text: TextSpan(
        // Note: Styles for TextSpans must be explicitly defined.
        // Child text spans will inherit styles from parent
        style: TextStyle(
          fontSize: 14.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          TextSpan(text: 'von '),
          TextSpan(
              text: author, style: TextStyle(fontWeight: FontWeight.bold)),
        ],
      ),
    );
  }
}
