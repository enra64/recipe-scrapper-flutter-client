import 'package:flutter/material.dart';
import 'package:loading_gifs/loading_gifs.dart';

class RecipeImage extends StatelessWidget {
  const RecipeImage({this.url}) : super();

  final String? url;

  @override
  Widget build(BuildContext context) {
    final url = this.url;
    if (url == null) {
      return Image(image: AssetImage("assets/gabriel-gurrola-nYE0WcaVRRw-unsplash.jpg"));
    } else {
      return FadeInImage.assetNetwork(
        placeholder: circularProgressIndicator,
        image: url,
      );
    }
  }
}
