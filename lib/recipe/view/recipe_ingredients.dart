import 'package:flutter/material.dart';

class RecipeIngredients extends StatelessWidget {
  const RecipeIngredients({this.ingredients}) : super();

  final List<String>? ingredients;

  @override
  Widget build(BuildContext context) {
    final ingredients = this.ingredients;
    if (ingredients == null) {
      return const Center(child: Text('Unknown Ingredients'));
    }

    final List<Widget> widgetList = [];
    widgetList
        .add(Text("Zutaten", style: Theme.of(context).textTheme.headline6));

    ingredients.forEach((element) => widgetList.add(Text(element)));

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: widgetList,
    );
  }
}
