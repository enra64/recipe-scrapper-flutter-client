import 'package:flutter/material.dart';
import 'package:recipe_app/api/generated/recipes.swagger.dart';

class RecipeInstructions extends StatelessWidget {
  const RecipeInstructions({this.recipe}) : super();

  final RecipeResponse? recipe;

  @override
  Widget build(
    BuildContext context,
  ) {
    final recipe = this.recipe;
    if (recipe == null) {
      return const Center(child: Text('Missing Recipe'));
    }

    _scrollable(Widget what) {
      return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: viewportConstraints.maxHeight,
              ),
              child: IntrinsicHeight(
                child: Column(
                  children: <Widget>[
                    Expanded(
                      // A flexible child that will grow to fit the viewport but
                      // still be at least as big as necessary to fit its contents.
                      child: what,
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    }

    return _scrollable(Padding(
      padding: EdgeInsets.all(4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text("Zubereitung", style: Theme.of(context).textTheme.headline3),
          SizedBox(height: 2),
          Text(recipe.instructions ?? "Unknown Instructions",
              style: Theme.of(context).textTheme.bodyText2)
        ],
      ),
    ));
  }
}
