import 'package:flutter/material.dart';
import 'package:recipe_app/api/generated/recipes.swagger.dart';
import 'package:recipe_app/recipe/view/recipe_author.dart';
import 'package:recipe_app/recipe/view/recipe_image.dart';
import 'package:recipe_app/recipe/view/recipe_ingredients.dart';
import 'package:recipe_app/recipe/view/recipe_name.dart';

class RecipeMeta extends StatelessWidget {
  const RecipeMeta({this.recipe}) : super();

  final RecipeResponse? recipe;

  _scrollable(Widget what) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: viewportConstraints.maxHeight,
            ),
            child: IntrinsicHeight(
              child: Column(
                children: <Widget>[
                  Expanded(
                    // A flexible child that will grow to fit the viewport but
                    // still be at least as big as necessary to fit its contents.
                    child: what,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    final recipe = this.recipe;
    if (recipe == null) {
      return const Center(child: Text('Missing Recipe'));
    }

    return _scrollable(
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            RecipeImage(url: recipe.image),
            Padding(
              padding: EdgeInsets.all(8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  RecipeName(recipeName: recipe.name),
                  SizedBox(height: 2,),
                  RecipeAuthor(author: recipe.author),
                  SizedBox(height: 16,),
                  RecipeIngredients(ingredients: recipe.ingredients)
                ],
              ),
            )
          ],
        )
    );
  }
}
