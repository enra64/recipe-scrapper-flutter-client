import 'package:flutter/material.dart';

class RecipeName extends StatelessWidget {
  const RecipeName({this.recipeName}) : super();

  final String? recipeName;

  @override
  Widget build(BuildContext context) {
    final name = this.recipeName;

    return Text(
      name ?? "Unknown Recipe Name",
      style: Theme.of(context).textTheme.headline5,
    );
  }
}
