import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recipe_app/clipboard_ingestion/bloc/clipboard_bloc.dart';
import 'package:recipe_app/recipe/recipe.dart';

class RecipePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ClipboardBloc>(context).stream.listen((state) {
      final url = state.clipboardContent;
      if (url != null) {
        BlocProvider.of<RecipeBloc>(context).add(RecipeRequested(url: url));
      }
    });

    return Scaffold(
      appBar: AppBar(
        title: const Text('Recipe Viewer'),
        actions: [
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              BlocProvider.of<ClipboardBloc>(context)
                  .add(ClipboardLoadRequested());
            },
          )
        ],
      ),
      body: BlocBuilder<RecipeBloc, RecipeState>(builder: (context, state) {
        switch (state.status) {
          case RecipeRetrievalStatus.failure:
            return const Center(child: Text('failed to fetch recipe'));
          case RecipeRetrievalStatus.success:
            if (state.recipe == null) {
              return const Center(child: Text('no recipe gotten'));
            }
            return OrientationBuilder(builder: (context, orientation) {
              if (orientation == Orientation.portrait) {
                return buildPortrait(state);
              } else {
                return buildLandscape(state);
              }
            });
          default:
            return const Center(child: CircularProgressIndicator());
        }
      }),
    );
  }

  buildPortrait(state) {
    return Column(
      children: [
        Flexible(
          flex: 1,
          child: RecipeMeta(recipe: state.recipe),
        ),
        Flexible(
          flex: 2,
          child: RecipeInstructions(recipe: state.recipe),
        ),
      ],
    );
  }

  buildLandscape(state) {
    return Row(
      children: [
        Flexible(
          flex: 1,
          child: RecipeMeta(recipe: state.recipe),
        ),
        Flexible(
          flex: 2,
          child: RecipeInstructions(recipe: state.recipe),
        ),
      ],
    );
  }
}
